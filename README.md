# Chemometrics Software

This repository contains software for chemometrics and multivariate analysis
The data used for demostration is taken from other repositories, such as https://gitlab.com/vfonsecad/chemometrics_data

Folders
- modules: python classes and functions for chemometrics software
- notebooks: demostrations and use of modules and other software available for python

Any contribution is very much welcome!