# ------------------------------------------------------------------------
#                           caltransfer methods
# by: Valeria Fonseca Diaz
#  ------------------------------------------------------------------------


import numpy as np
from sklearn.cross_decomposition import PLSRegression
from sklearn.linear_model import LinearRegression


def ds_pc_transfer_fit(X_primary, X_secondary, max_ncp):
    
    '''
    
    Transfer model with Direct Standardization (DS) based on PCR but normally with a lot of pcs
    X_primary: (np.array) spectral matrix from primary instrument or environment (N x K)
    X_secondary: (np.array) spectral matrix from secondary instrument or environment (N x K)
    max_ncp: (int) number of pc's to train DS model. For classical DS, max_ncp = K
    
    output: (F,b) for X_primary = X_secondary*F + b
    
    '''

    kk = X_primary.shape[1]
    N = X_primary.shape[0]
    ww = 2*kk


    F = np.zeros((kk,kk))
    b = np.zeros((1,kk))

    mean_primary = X_primary.mean(axis=0)
    mean_secondary = X_secondary.mean(axis=0)
    X_primary_c = X_primary - mean_primary
    X_secondary_c = X_secondary - mean_secondary

    for jj_out in range(0,kk):

        # --- wv to predict    


        X_out = X_primary_c[:, jj_out:jj_out+1]     


        # --- input matrix

        ll = np.amax([0, jj_out - ww])
        ul = np.amin([jj_out + ww, kk-1])
        jj_in = np.arange(ll, ul+1)
        X_in = X_secondary_c[:, jj_in]

        current_ncp = np.amin([max_ncp,X_in.shape[1]])
        

        # ------ svd

        U0,S,V0t = np.linalg.svd(X_in.T.dot(X_in),full_matrices=True)
        S_matrix = np.zeros((current_ncp,current_ncp))
        S_matrix[0:current_ncp,:][:,0:current_ncp] = np.diag(S[0:current_ncp])
        V = V0t[0:current_ncp,:].T
        U = U0[:,0:current_ncp]

        # --- regression    

        F_current = U.dot(np.linalg.inv(S_matrix)).dot(V.T).dot(X_in.T).dot(X_out)

        F[jj_in,jj_out] = F_current[:,0]




    b[0,:] = mean_primary - F.T.dot(mean_secondary)
    
    return (F,b)


    


def pds_pls_transfer_fit(X_primary, X_secondary, max_ncp, ww):
    
    '''
    
    Transfer model with (Piecewise) Direct Standardization (PDS) based on PLSR 
    X_primary: (np.array) spectral matrix from primary instrument or environment (N x K)
    X_secondary: (np.array) spectral matrix from secondary instrument or environment (N x K)
    max_ncp: (int) number of pc's to train PDS model. For classical DS, max_ncp = K
    ww: half of the total window width. The total window width is 2*ww + 1. For classical DS, ww may be set to K.
    
    output: (F,b) for X_primary = X_secondary*F + b
    
    '''

    kk = X_primary.shape[1]
    N = X_primary.shape[0]
   


    F = np.zeros((kk,kk))
    b = np.zeros((1,kk))

    mean_primary = X_primary.mean(axis=0)
    mean_secondary = X_secondary.mean(axis=0)
    X_primary_c = X_primary - mean_primary
    X_secondary_c = X_secondary - mean_secondary

    for jj_out in range(0,kk):

        # --- wv to predict    


        X_out = X_primary_c[:, jj_out]     


        # --- input matrix

        ll = np.amax([0, jj_out - ww])
        ul = np.amin([jj_out + ww, kk-1])
        jj_in = np.arange(ll, ul+1)
        X_in = X_secondary_c[:, jj_in]

        chosen_lv = np.amin([max_ncp,X_in.shape[1]])
        
        # --- pls transfer
        
 
        my_pls = PLSRegression(n_components = chosen_lv,scale=False)
        my_pls.fit(X_in, X_out)
        
 
        F[jj_in,jj_out] = my_pls.coef_[:,0]




    b[0,:] = mean_primary - F.T.dot(mean_secondary)
    
    return (F,b)


def epo_fit(x_group1, x_group2,current_epo_ncp=1):
    
    '''
    this function performs EPO on 2 groups of paired spectra

    x_group1: old matrix of coupled spectra
    x_group2: new matrix of coupled spectra
    current_epo_ncp: 1. Number of EPO components to remove
    

    output: (EPO_filter, x_ref)
    EPO_filter: Matrix EPO filter
    x_ref: shift or offset for EPO filter
    
    '''
    
    

    
    
    D = x_group1 - x_group2


    U0,S,V0t = np.linalg.svd(D)
    S_matrix = np.zeros((current_epo_ncp,current_epo_ncp))
    S_matrix[0:current_epo_ncp,:][:,0:current_epo_ncp] = np.diag(S[0:current_epo_ncp])
    V = V0t[0:current_epo_ncp].T
    U = U0[:,0:current_epo_ncp]


    EPO_filter_inst_diff = np.identity(n=V.shape[0]) - V.dot(V.T)
    

    
    return (EPO_filter_inst_diff, x_group1.mean(axis=0))


def jointpls_regression(x_primary_tr, x_secondary_tr, y_primary, y_secondary, plsr_basemodel):
    
    '''
    plsr_basemodel is a python object having all the parameters of a PLSR model
    x_primary_tr,x_secondary_tr: x_primary samples after any possible transformation, e.g. DS, PDS or EPO when it applies

    
        
    '''
 
    x_primary_tr_preproc = plsr_basemodel.preprocessing(xx=x_primary_tr)
    x_secondary_tr_preproc = plsr_basemodel.preprocessing(xx=x_secondary_tr)


    tscores_primary = (x_primary_tr_preproc - plsr_basemodel.x_mean).dot(plsr_basemodel.R)
    tscores_secondary = (x_secondary_tr_preproc - plsr_basemodel.x_mean).dot(plsr_basemodel.R)


    t_jointpls = np.concatenate((tscores_primary,tscores_secondary), axis = 0)
    y_jointpls = np.concatenate((y_primary,y_secondary), axis = 0)


    calmodel_tr_jointpls = LinearRegression()
    calmodel_tr_jointpls.fit(t_jointpls,y_jointpls) # output values need to be 1D always here
    
            # output
    
    q_mod = calmodel_tr_jointpls.coef_ 
    B_mod = np.dot(plsr_basemodel.R,q_mod)   
    b0 = calmodel_tr_jointpls.intercept_
    beta_mod = np.asarray(b0 - (plsr_basemodel.x_mean).dot(B_mod))
    beta_mod.shape = plsr_basemodel.beta.shape
    
    return (q_mod, B_mod, beta_mod,b0)




def nipals_dipls(X,y_uni,Xs,Xt,nlv,lambda_, mode = 0):
    
    
    '''
    Algorithm based on: 
    Domain-InvariantPartialLeastSquares(di-PLS)Regression
    https://pubs.acs.org/doi/abs/10.1021/acs.analchem.8b00498
    Algorithm: https://pubs.acs.org/doi/suppl/10.1021/acs.analchem.8b00498/suppl_file/ac8b00498_si_001.pdf
    mode = int 0,1,2. 0: unsupervised, 1: (semi)supervised, 2: caltransfer. See reference paper Table 1. 
    Here, even modes 0 and 1 can be used for calibration transfer
    returns all model coefficients return (B,R,Q,beta,x_mu_target_torecenter,b0)
    '''
    
    assert y_uni.ndim < 2 or y_uni.shape[1] == 1, "this algorithm is for univariate response"

    
    y = y_uni.copy()
    
    if y.ndim == 1:
        y.shape = (y.shape[0], 1)

    
    x_mu = np.mean(X, axis=0)
    x_mu_source = np.mean(Xs, axis=0)
    x_mu_target = np.mean(Xt, axis=0)
    
    ns = Xs.shape[0]
    nt = Xt.shape[0]
    n = X.shape[0]
    k = X.shape[1]
    ky = y.shape[1]
    
    
    # centering x's
    X_c = X - x_mu
    Xs_c = Xs - x_mu_source
    Xt_c = Xt - x_mu_target
    
    # cov matrices
    
    cov_Xs = (np.dot(Xs_c.T, Xs_c))/(ns-1)
    cov_Xt = (np.dot(Xt_c.T, Xt_c))/(nt-1)
    
    # setting the mean to which test samples should be recentered
    
    if mode == 0:       
        
        assert y.shape[0] == Xs.shape[0], "for unsupervised, X and Xs must be the same"
        
        x_mu_target_torecenter = np.mean(Xt, axis=0)
         
    elif mode == 1:
        
        x_mu_target_torecenter = np.mean(X, axis=0)
        
    elif mode == 2:
        
        x_mu_target_torecenter = np.mean(Xt, axis=0)
        
    
    
    b0 = np.mean(y,axis=0)
    
    y_c = y - b0 # centering y

    

    
    
    # store output matrices
    
    T = np.zeros((n, nlv))
    Ts = np.zeros((ns, nlv))
    Tt = np.zeros((nt, nlv))
    
    P = np.zeros((k, nlv))
    Ps = np.zeros((k, nlv))
    Pt = np.zeros((k, nlv))
    
    Wdi = np.zeros((k, nlv)) 
    
    Q = np.zeros((nlv,ky))
    
    for a in range(nlv):
        
        IXSXT = np.linalg.inv(np.eye(k) + lambda_*(1/2*np.dot(y_c.T,y_c))*(cov_Xs - cov_Xt))
        w = np.dot(np.dot(y_c.T,X_c)/np.dot(y_c.T,y_c),IXSXT).T
        w = w/np.sqrt(np.dot(w.T,w))        
        
        tt = np.dot(X_c,w)
        tts = np.dot(Xs_c,w)
        tt_t = np.dot(Xt_c,w)
        
        p = (1/np.dot(tt.T,tt))*np.dot(tt.T,X_c)
        ps = (1/np.dot(tts.T,tts))*np.dot(tts.T,Xs_c)
        pt = (1/np.dot(tt_t.T,tt_t))*np.dot(tt_t.T,Xt_c) 
        
        q = (1/np.dot(tt.T,tt))*np.dot(tt.T,y_c)
        
        # deflation
 
        X_c -= np.dot(tt,p)
        Xs_c -= np.dot(tts,ps)
        Xt_c -= np.dot(tt_t,pt)
        
        y_c -= q*tt
        
        T[:,a] = tt.flatten()
        Ts[:,a] = tts.flatten()
        Tt[:,a] = tt_t.flatten()
        
        P[:,a] = p.flatten()
        Ps[:,a] = ps.flatten()
        Pt[:,a] = pt.flatten()
        
        Wdi[:,a] = w.flatten()
        Q[a,:] = q.flatten()
        
        
    # loadings for X
    

 
    PWI = np.linalg.inv(np.dot(P.T, Wdi))

    R = np.dot(Wdi, PWI)
    
    B = np.dot(R,Q)
    
    beta = b0 - np.dot(x_mu_target_torecenter,B)
    
        
    return (B,R,Q,beta,x_mu_target_torecenter,b0)


