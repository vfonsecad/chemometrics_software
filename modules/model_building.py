# ------------------------------------------------------------------------
#                           model building functions
# by: Valeria Fonseca Diaz
#  ------------------------------------------------------------------------


import numpy as np
from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
import matplotlib as mtply



def plsr_univariate_optimal_lv(xcal_pls, ycal, total_lv, n_splits_cv, n_shuffles,quantile_limit = 0.75, cv_plot = False,random_seed = None):
    
	'''
	Automatic selection of optimal number of lv for PLS Regression based on median RMSECV upon several crossvalidation shuffles
	xcal_pls, ycal, total_lv, cv_splits, n_shuffles

	xcal_pls: 2D array N X K
	ycal: 1D array N
	total_lv: int. total number of lv to evaluate
	cv_splits: int. number of splits for crossvalidation
	n_shuffles: int. number of repetitions for crossvalidation shuffling the order each time randomly
	quantile_limit: float between 0.5 and 1. Quantile to set the threshold for the RMSECV
	cv_plot: boolean. False by default. Plot the CV repetitions 

	output: int. chosen_lv

	'''

	assert ycal.ndim == 1 or ycal.shape[1] == 1, "ycal must be 1D"

	ycal = ycal.flatten()

	if random_seed is None:

		random_seeds = [None for nn in range(n_shuffles)]
	else:

		rng = np.random.default_rng(random_seed)
		random_seeds = rng.choice(65443876, n_shuffles, replace = False)


	parameters = {'n_components': list(range(1,total_lv+1))}
	pls_scoring = {'rmse': 'neg_root_mean_squared_error'}

	rmsecv = np.zeros((n_shuffles, total_lv))

	for nn in range(n_shuffles):

	    pls_cv = KFold(n_splits=n_splits_cv, shuffle=True, random_state = random_seeds[nn])
	    my_pls = PLSRegression(scale=False)
	    my_pls_cv = GridSearchCV(my_pls, parameters, cv = pls_cv, scoring = pls_scoring, refit = False)
	    my_pls_cv.fit(xcal_pls, ycal)

	    rmsecv[nn,:] = -1*my_pls_cv.cv_results_["mean_test_rmse"]
	    

	rmsecv_median = np.median(rmsecv, axis = 0)
	rmsecv_ul = np.quantile(rmsecv,quantile_limit, axis = 0)

	lv_min_loc = np.argmin(rmsecv_median)
	rmsecv_accepted = rmsecv_ul[lv_min_loc]

	final_lv = np.min(np.where(rmsecv_median<=rmsecv_accepted)[0]) + 1


	if cv_plot:
	    
	    
	    plt.plot(np.arange(1, total_lv+1),rmsecv.T, c = "gray")
	    plt.plot(np.arange(1, total_lv+1),rmsecv_median, c = "red", label = "median curve")
	    plt.plot(np.arange(1, total_lv+1),rmsecv_ul, c = "green", label = "upper limit curve")
	    plt.plot(np.arange(1, total_lv+1),np.repeat(rmsecv_accepted,total_lv), c = "black", label = "upper limit value")
	    plt.plot([final_lv],[rmsecv_accepted],'o', markerSize = 10, c  ="blue", label = "optimal lv")
	    plt.xlabel("lv")
	    plt.ylabel("rmsecv")
	    plt.title("choice of optimal lv")
	    plt.legend()
	    plt.grid()
	    plt.show()


	return final_lv


def plsr_univariate_cv_repetitions(xcal_pls, ycal, total_lv, n_splits_cv, n_shuffles,quantile_limit = 0.75, cv_plot = False,random_seed = None):
    
	'''
	Automatic selection of optimal number of lv for PLS Regression based on median RMSECV upon several crossvalidation shuffles
	xcal_pls, ycal, total_lv, cv_splits, n_shuffles

	xcal_pls: 2D array N X K
	ycal: 1D array N
	total_lv: int. total number of lv to evaluate
	cv_splits: int. number of splits for crossvalidation
	n_shuffles: int. number of repetitions for crossvalidation shuffling the order each time randomly
	quantile_limit: float between 0.5 and 1. Quantile to set the threshold for the RMSECV
	cv_plot: boolean. False by default. Plot the CV repetitions 

	output: rmsecv_median,plot

	'''

	assert ycal.ndim == 1 or ycal.shape[1] == 1, "ycal must be 1D"

	ycal = ycal.flatten()

	if random_seed is None:

		random_seeds = [None for nn in range(n_shuffles)]
	else:

		rng = np.random.default_rng(random_seed)
		random_seeds = rng.choice(65443876, n_shuffles, replace = False)


	parameters = {'n_components': list(range(1,total_lv+1))}
	pls_scoring = {'rmse': 'neg_root_mean_squared_error'}

	rmsecv = np.zeros((n_shuffles, total_lv))

	for nn in range(n_shuffles):
	

		pls_cv = KFold(n_splits=n_splits_cv, shuffle=True, random_state = random_seeds[nn])
		my_pls = PLSRegression(scale=False)
		my_pls_cv = GridSearchCV(my_pls, parameters, cv = pls_cv, scoring = pls_scoring, refit = False)
		my_pls_cv.fit(xcal_pls, ycal)

		rmsecv[nn,:] = -1*my_pls_cv.cv_results_["mean_test_rmse"]
	    

	rmsecv_median = np.median(rmsecv, axis = 0)
	rmsecv_ul = np.quantile(rmsecv,quantile_limit, axis = 0)


	if cv_plot:
	    
	    
	    plt.plot(np.arange(1, total_lv+1),rmsecv.T, c = "gray")
	    plt.plot(np.arange(1, total_lv+1),rmsecv_median, c = "red", label = "median curve")
	    plt.plot(np.arange(1, total_lv+1),rmsecv_ul, c = "green", label = "upper limit curve")
	    plt.xlabel("lv")
	    plt.ylabel("rmsecv")
	    plt.title("cv repetitions to choose optimal lv")
	    plt.legend()
	    plt.grid()
	    plt.show()


	return rmsecv_median

        
def pls_univariate_cv(xx,yy,lv):
    
    '''
    function just to calculate the rmsecv value with 10 splits and no shuffle and fixed number of lv    
    '''
    
    parameters = {'n_components': [lv]}
    pls_scoring = {'r2': 'r2', 'rmse': 'neg_root_mean_squared_error'}
    pls_cv = KFold(n_splits=10, shuffle=False)
    my_pls = PLSRegression(scale=False)
    my_pls_cv = GridSearchCV(my_pls, parameters, cv = pls_cv, scoring = pls_scoring, refit = False)
    my_pls_cv.fit(xx, yy)
    rmsecv = -1*my_pls_cv.cv_results_["mean_test_rmse"]
    
    return rmsecv[0]



def pls_univariate_varselection(xx, yy, chosen_lv,interval_size, forward = True):


	'''
	performs variable selection with interval split of spectral range.
	the selection can be done forward (True) or backward (forward = False)
	'''

	N = yy.shape[0]
	kk2 = xx.shape[1]

    
	# --- variable selection

	    # --- intervals id

	wv_init = 0
	ww = int(interval_size/2)
	intervals_id = np.zeros(kk2, dtype=np.int64)


	if (wv_init-ww) >= 0:
	    wv_init_ll = wv_init-ww
	else:
	    wv_init_ll = 0
	    
	if (wv_init+ww+1) <= kk2:
	    wv_init_ul = wv_init+ww+1
	else:
	    wv_init_ul = kk2
	    
	  
	intervals_id[wv_init_ll:wv_init_ul] = 1

	current_wv = wv_init_ul
	ss = 2
	while current_wv < kk2:
	    intervals_id[current_wv:(current_wv+interval_size)] = ss
	    current_wv += interval_size
	    ss += 1

	current_wv = 0
	while current_wv < wv_init_ll-interval_size:
	    intervals_id[current_wv:(current_wv+interval_size)] = ss
	    current_wv += interval_size
	    ss += 1
	intervals_id[intervals_id==0] = ss


	unique_intervals = np.sort(np.unique(intervals_id))

	    # --- loop interval selection

	nonstop = True
	intervals_inside = np.zeros(unique_intervals.shape[0], dtype=np.int64)

	current_rmsecv_final = 10*yy.std()
	selected_ii = 0
	itt = 0

	while nonstop:

	    current_rmsecv = current_rmsecv_final
	    print("rmsecv: ", current_rmsecv_final)

	    for ii in unique_intervals[intervals_inside==0]:
	        
	        

	        current_intervals = np.append(intervals_inside[intervals_inside!=0], ii)
	        current_intervals_bool = np.zeros(kk2, dtype=np.int64) == 1

	        for jj in range(kk2):
	            if np.sum(current_intervals == intervals_id[jj])>0:
	                current_intervals_bool[jj] = True

	        
	        
	        if chosen_lv > np.sum(current_intervals_bool):
	            chosen_lv_cv = np.sum(current_intervals_bool)
	        else:
	            chosen_lv_cv = chosen_lv
	        
	        current_rmsecv_iter = pls_univariate_cv(xx[:,current_intervals_bool==forward], yy, chosen_lv_cv)

	        
	        if current_rmsecv_iter <= current_rmsecv:
	            current_rmsecv = current_rmsecv_iter
	            selected_ii = ii
	           

	    intervals_inside[unique_intervals==selected_ii] = selected_ii

	    if current_rmsecv >= current_rmsecv_final or itt >= unique_intervals[-1]:
	        nonstop = False
	    itt += 1
	    current_rmsecv_final = current_rmsecv
	    
	print("rmsecv: ", current_rmsecv_final)

	# --- final intervals

	intervals_inside_bool = np.zeros(kk2, dtype=np.int64) == 1
	for jj in range(kk2):
	    if np.sum(intervals_inside[intervals_inside!=0]==intervals_id[jj])>0:
	        intervals_inside_bool[jj] = True



	return intervals_inside_bool==forward