# Chemometrics Notebooks

Here's a compilation of protocols or routines for different objectives that other users can adopt and modify to their needs

**Notebook** | Description
--- | ---
**caltransfer_protocol** | Routine for calibration transfer using methods such as DS, PDS, EPO and joint PLS
**caltransfer_protocol_preprocessing** | Similar calibration transfer routine adding a step for preprocessing before transfer
**pls_sklearn** | Calibration regression model building using PLSR from sklearn
**preprocessing_search** | Routine to test a vast number of preprocessing combinations and summarize their performance for regression models with PLSR
**unsuperv_sample_selection_comparison** | Comparison of Kennard Stone, Duplex, Puchwein, Clustering and D-optimal designs for unsupervised sample selection to build calibration models